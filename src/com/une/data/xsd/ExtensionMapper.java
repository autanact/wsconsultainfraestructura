
/**
 * ExtensionMapper.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:41 LKT)
 */

            package com.une.data.xsd;
            /**
            *  ExtensionMapper class
            */
        
        public  class ExtensionMapper{

          public static java.lang.Object getTypeObject(java.lang.String namespaceURI,
                                                       java.lang.String typeName,
                                                       javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{

              
                  if (
                  "http://ws.une.com".equals(namespaceURI) &&
                  "RespuestaProceso".equals(typeName)){
                   
                            return  com.une.ws.RespuestaProceso.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://ws.une.com".equals(namespaceURI) &&
                  "ParametrosConsultaInfraestructura".equals(typeName)){
                   
                            return  com.une.ws.ParametrosConsultaInfraestructura.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://ws.une.com".equals(namespaceURI) &&
                  "Elementos".equals(typeName)){
                   
                            return  com.une.ws.Elementos.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://ws.une.com".equals(namespaceURI) &&
                  "Elemento".equals(typeName)){
                   
                            return  com.une.ws.Elemento.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://ws.une.com".equals(namespaceURI) &&
                  "RespuestaConsultaInfraestructura".equals(typeName)){
                   
                            return  com.une.ws.RespuestaConsultaInfraestructura.Factory.parse(reader);
                        

                  }

              
             throw new org.apache.axis2.databinding.ADBException("Unsupported type " + namespaceURI + " " + typeName);
          }

        }
    