
/**
 * WSConsultaInfraestructuraSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.3  Built on : Aug 10, 2007 (04:45:47 LKT)
 * with extensions for GE Smallworld GeoSpatial Server
 */
    package com.une.ws;
    import java.util.Map;
    import java.util.HashMap;    
    
    import org.apache.axis2.engine.AxisError;
    
    import com.gesmallworld.gss.lib.exception.GSSException;
    import com.gesmallworld.gss.webservice.WebServiceRequest;
    /**
     *  WSConsultaInfraestructuraSkeleton java skeleton for the axisService
     */
    public class WSConsultaInfraestructuraSkeleton extends WebServiceRequest
        {
        
	
	private static final String serviceName = "ejb/WSConsultaInfraestructuraLocal";
	
     
         
        /**
         * Auto generated method signature
         
         
                                     * @param parametros
         */
        

                 public com.une.ws.ConsultaInfraestructuraResponse consultaInfraestructura
                  (
                  com.une.ws.ParametrosConsultaInfraestructura parametros
                  )
            {
                //GSS generated code
		Map<String,Object> params = new HashMap<String,Object>();
                  params.put("parametros",parametros);
		try{
		
			return (com.une.ws.ConsultaInfraestructuraResponse)
			this.makeStructuredRequest(serviceName, "consultaInfraestructura", params);
		}catch(GSSException e){
                    // Modify if specific faults are required
                    throw new AxisError(e.getLocalizedMessage()+": "+e.getRootThrowable().getLocalizedMessage(), e.getRootThrowable());
                }
        }
     
    }
    